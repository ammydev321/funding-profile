package com.fusion.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FusionFundingProfileApplication {

	public static void main(String[] args) {
		SpringApplication.run(FusionFundingProfileApplication.class, args);
	}

}
