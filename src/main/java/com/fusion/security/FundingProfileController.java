package com.fusion.security;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/v1/funding-profile")
public class FundingProfileController
{

	@GetMapping(path = "/", produces = "application/json")
	public ResponseEntity<String> getFundingProfile()
	{
		return new ResponseEntity<>("This is funding profile ", HttpStatus.OK);
	}

}
